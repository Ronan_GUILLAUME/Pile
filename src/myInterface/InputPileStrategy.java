package myInterface;

/**
 * Created by ronan
 * on 09/01/2018.
 */
public interface InputPileStrategy {
    void push(int integer);
    int pop();
    void clear();
    void actionCommand();
}
