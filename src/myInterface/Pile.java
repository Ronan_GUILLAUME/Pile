package myInterface;

/**
 * Created by ronan
 * on 09/01/2018.
 */
public interface Pile {
    int getSizeList();
    int getInteger(int index);
    int pop();
    void clear();
}
