package myInterface;

/**
 * Created by ronan
 * on 09/01/2018.
 */
public interface Observer {
    void update(Observable o, Object arg);
}
