package myInterface;

/**
 * Created by ronan
 * on 09/01/2018.
 */
public interface Observable {
    void setChanged();
    void notifyObservers();
}
